# Structured Web Markup Language

These are the files defining the Structured Web Markup Language *(SWML)*.

This repository is divided in three major folders:

```
/ref - The SWML and standard libraries reference (for creating SWML)
/doc - The SWML and standard libraries documentation (for SWML developers)
/tuto - Tutorial and Introduction to SWML (for beginners in SWML)
```
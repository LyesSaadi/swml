# Structure

## Indentation

## Example

```
tree:
    // Comment
    %b1:
        %nested_one_line_block(value1, param2: value2)
        #style: value
        $param: value
        !statement: value
        Split \
        Text
    #rootstyle: value
    $rootparam: value
    !rootstatement: value
    Text
metadata:
    md: value
    stylesheet: style.css
definition:
    %nested_one_line_block(param1: default, param2):
        !param: &param1 + &param2
import:
    block1, block2 as b1, n/b2: <stdlib> // Import in current namespace and in "n" namespace
    * as f: file.swml // Import in f namespace
    definitions.swml // Import in current namespace
```
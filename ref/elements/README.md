# Elements

In SWML there is 4 types of elements:
- blocks: `/^(?P<inden>(?: |\t)+)?%(?P<name>[a-zA-Z]+)(?:\((?P<param>[^\(\)]*)\))?(?:(?P<sep>:)(?: *)?(?P<value>.+)?)?$/`
- styles: `/^(?P<inden>(?: |\t)+)?#(?P<name>[a-zA-Z]+)(?:(?P<sep>:)(?: *)?(?P<value>.+)?)?$/`
- parameters: `/^(?P<inden>(?: |\t)+)?\$(?P<name>[a-zA-Z]+)(?:(?P<sep>:)(?: *)?(?P<value>.+)?)?$/` (inputs) and `/&(?P<name>[a-zA-Z]+)/` (outputs)
- statements: `/^(?P<inden>(?: |\t)+)?!(?P<name>[a-zA-Z]+)(?:(?P<sep>:)(?: *)?(?P<value>.+)?)?$/`
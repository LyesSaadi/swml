# Reference

A SWML code is divided in 4 distinct parts, called « root elements »:

- tree: It's the tree of the document. It's the only mandatory root element.
- metadata: It's the metadatas of the document. It's the equivalent of the head element in HTML.
- definition: It's where all custom blocks are defined.
- import: It's where all external definitions are imported.

Their reference is located in these files:

```
/ref/tree.md - The tree part
/ref/metadata.md - The metadata part
/ref/definition.md - The definition part
/ref/import.md - The import part
```

To see the basic structure of a SWML file, go to `/ref/structure.md`
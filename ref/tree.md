# Tree

The tree is composed of blocks. The tree is in itself a block; it represents the page where all elements are. Changing the background color of the tree change the entire page background color.

Unless a `from` statement is declared (see `/ref/elements/statements.md`), the tree block has absolutely no initial property. It is just a blank page.

## Blocks

Blocks in the tree are nested using indentation (see `/ref/structure/structure.md#indentation`).

Blocks are defined inside the definition part (see `/ref/definition.md`) or imported (see `/ref/import.md`). It is possible to add non-blocks statements inside the tree part and extend/overwrite existing definition but it's only recommended if it's specific to a single block, like cards with different colors/images. But all blocks have to be defined or imported. Blank blocks can be used to construct everything in the tree, but they have to be defined nonetheless (see `/ref/definition.md#blank-blocks`).